$(document).ready(function(){
	function defaultSettings(){
		$('.mobile-nav-content-overley').css('display', 'none');
		$('#mobile-nav').css('right', '-9999px').removeClass('open');
		$('.page').css('left', '0');
	}
	function openMenu(){
		$('.mobile-nav-content-overley').css('display', 'block');
        $('#mobile-nav').animate({'right': 0}, 400).addClass('open');
        $('.page').animate({'left': -285}, 400);
	}
	function closeMenu(){
		$('.mobile-nav-content-overley').css('display', 'none');
		$('#mobile-nav').animate({'right': -9999}, 400).removeClass('open');
		$('.page').animate({'left': 0}, 400);
	}
	$('#mob-nav-control').click(function(){
		openMenu();
    });
	$('.mobile-nav-content-overley').click(function(){
		closeMenu();
	});
	$(window).resize(function(){
		var width = $(window).width();
		if(width > 620){
			defaultSettings()
		}else{}
	});
	$('#mobile-nav .show-hide-btn').click(function(){
		$(this).toggleClass('open');
		$(this).parent('li').find('.drop').slideToggle();
	});
	$('.footer-links .show-hide-btn').click(function(){
		$(this).toggleClass('open');
		$(this).parents('.item').find('ul').slideToggle();
	});
	$('.footer-links h2').click(function(){
		$(this).next().toggleClass('open');
		$(this).parents('.item').find('ul').slideToggle();
	});
	$('.top-menu > li').hover(
		function(){
			$(this).find('.drop-menu').show();
		},
		function(){
			$(this).find('.drop-menu').hide();
	});
	$('.top-menu > li').each(function(){
		var listItem = $(this);
		listItem.find('.drop-menu').parents('li').addClass('nav-drop-parent');
	});
	$('.top-search').hover(
		function(){
			$(this).addClass('focus');
			$(this).find('input').focus();
		},
		function(){
			$(this).removeClass('focus');
	});
	$('.account').hover(
		function(){
			$(this).find('.account-drop').addClass('active');
		},
		function(){
			$(this).find('.account-drop').removeClass('active');
	});
	$('.mini-cart').hover(
		function(){
			$(this).find('.mini-cart-drop').addClass('active');
		},
		function(){
			$(this).find('.mini-cart-drop').removeClass('active');
	});
	$('.cart-body .remove-btn').click(function(){
		$(this).parents('li').remove();
	});
	/*slider begin*/
	
	var cnt_slider = 0;
	var cnt_pagin = 0;
	var slider_item = $('.slider li').index();
	var pagin_item = $('.pagination li').index();
	
	$('.slider li').each(function(){
		cnt_slider++;
		$(this).addClass('item-'+cnt_slider);
	}); 
	$('.pagination li').each(function(){
		cnt_pagin++;
		$(this).addClass('item-'+cnt_pagin);
	}); 
		
	$('.pagination li').click(function(){
		
		$('.pagination li').removeClass('current');
		$(this).addClass('current');
	});
	
	/*slider end*/
});

